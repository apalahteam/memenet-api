'use strict';

const bcrypt = require('bcrypt');

const firstNames = ['Anna', 'Bryan', 'Camellia', 'Dean', 'Ellie', 'Faustine', 'Gina', 'Helga', 'Iliana', 'Joyce'];
const lastNames = ['Brown', 'Smith', 'Black', 'Johnson', 'Taylor', 'Miller', 'Harris', 'Moore', 'Lewis', 'Robinson', 'Anderson'];

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    let users = [{
      id_user_level: 1,
      email: 'admin@mail.com',
      password: bcrypt.hashSync('admin123', 10),
      name: 'Admin',
      created_at: new Date()
    }];

    for (let i = 1; i <= 10; i++) {
      let firstName = firstNames[Math.floor(Math.random() * firstNames.length)];
      let lastName = lastNames[Math.floor(Math.random() * lastNames.length)];
      let user = {
        id_user_level: 2,
        email: `${firstName.toLowerCase()}.${lastName.toLowerCase()}@mail.com`,
        password: bcrypt.hashSync('pass123', 10),
        name: `${firstName} ${lastName}`,
        created_at: new Date()
      };
      users.push(user);
    }
    return queryInterface.bulkInsert('user', users, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('user', null, {});
  }
};