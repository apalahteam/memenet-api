"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    let apiSites = [
      {
        id: 1,
        name: "Tenor",
        base_url: "https://api.tenor.com/v1",
        apikey: "A17CN64FUVQ1",
        created_at: new Date(),
      },
      {
        id: 2,
        name: "Giphy",
        base_url: "https://api.giphy.com/v1/gifs",
        apikey: "d2FjSJGbDS2Y4edpCoqtof3W1tlgqcXq",
        created_at: new Date(),
      },
    ];

    return queryInterface.bulkInsert("api_site", apiSites, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete("api_site", null, {});
  },
};
