'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    let projectPlans = [];
    let activeProjects = [];
    for (let i = 1; i <= 20; i++) {
      let id_project = Math.floor(Math.random() * 4) + 1;
      let id_plan = Math.floor(Math.random() * 4) + 1;
      let expires_in = Math.floor(Math.random() * 3) + i + 1;
      let expires_at = new Date();
      expires_at.setDate(expires_at.getDate() + expires_in);
      let is_active = (activeProjects.includes(id_project)) ? 0 : 1;

      let projectPlan = {
        id: i,
        id_project: id_project,
        id_plan: id_plan,
        expires_at: expires_at,
        is_active: is_active,
        remaining_hit: Math.floor(Math.random() * 2000) + 1,
        created_at: new Date(),
      };

      projectPlans.push(projectPlan);
      if (is_active == 1) activeProjects.push(id_project);
    }
    return queryInterface.bulkInsert('project_plan', projectPlans, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('project_plan', null, {});
  }
};