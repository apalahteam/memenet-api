'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    let userTopups = [{
      id_user: 1,
      nominal: 10000000,
      message: 'Topup',
      created_at: new Date()
    }];

    for (let i = 2; i <= 6; i++) {
      let userTopup = {
        id_user: i,
        nominal: 1000000,
        message: 'Topup',
        created_at: new Date()
      };
      userTopups.push(userTopup);
    }

    return queryInterface.bulkInsert('user_topup', userTopups, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('user_topup', null, {});
  }
};