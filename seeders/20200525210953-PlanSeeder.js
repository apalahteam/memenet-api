'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('plan', [{
        id: 1,
        name: 'Free',
        price: 0,
        duration: 30,
        max_hit_month: 2000,
      },
      {
        id: 2,
        name: 'Regular',
        price: 100000,
        duration: 30,
        max_hit_month: 5000
      },
      {
        id: 3,
        name: 'Extended',
        price: 200000,
        duration: 30,
        max_hit_month: 10000,
      },
      {
        id: 4,
        name: 'Premium',
        price: 400000,
        duration: 30,
        max_hit_month: 25000,
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('plan', null, {});
  }
};