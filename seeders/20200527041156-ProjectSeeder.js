'use strict';

const crypto = require("crypto");

function getSHA256ofJSON(input) {
  return crypto.createHash('sha256').update(JSON.stringify(input)).digest('hex');
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    let projects = [];
    for (let i = 1; i <= 5; i++) {
      let id_user = Math.floor(Math.random() * 2) + 1;
      let project = {
        id: i,
        id_user: id_user,
        name: `project${i}`,
        appkey: getSHA256ofJSON(`${id_user}${Date.now()}`),
        created_at: new Date(),
      }
      projects.push(project);
    }
    return queryInterface.bulkInsert('project', projects, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('project', null, {});
  }
};