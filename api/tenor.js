const request = require('request');
module.exports.search = (tenor_api, q, filter, locale) => {
    return new Promise((resolve, reject) => {
        let options = {
            'method': 'GET',
            'url': `${tenor_api.base_url}/search?key=${tenor_api.apikey}&q=${q}&contentfilter=${filter}&locale=${locale}`,
        };
        request(options, (error, response) => {
            if (error) reject(new Error(error));
            else {
                console.log(JSON.parse(response.body));
                resolve(JSON.parse(response.body));
            }
        });
    });
};

module.exports.trending = (tenor_api, filter, locale) => {
    return new Promise((resolve, reject) => {
        let options = {
            'method': 'GET',
            'url': `${tenor_api.base_url}/trending?key=${tenor_api.apikey}&contentfilter=${filter}&locale=${locale}`,
        };
        request(options, (error, response) => {
            if (error) reject(new Error(error));
            else {
                resolve(JSON.parse(response.body));
            }
        });
    });
};

module.exports.category = (tenor_api, filter, locale) => {
    return new Promise((resolve, reject) => {
        let options = {
            'method': 'GET',
            'url': `${tenor_api.base_url}/categories?key=${tenor_api.apikey}&contentfilter=${filter}&locale=${locale}`,
        };
        request(options, (error, response) => {
            if (error) reject(new Error(error));
            else {
                resolve(JSON.parse(response.body));
            }
        });
    });
};

module.exports.autocomplete = (tenor_api, q, filter, locale) => {
    return new Promise((resolve, reject) => {
        let options = {
            'method': 'GET',
            'url': `${tenor_api.base_url}/autocomplete?key=${tenor_api.apikey}&q=${q}&contentfilter=${filter}&locale=${locale}`,
        };
        request(options, (error, response) => {
            if (error) reject(new Error(error));
            else {
                resolve(JSON.parse(response.body));
            }
        });
    });
};