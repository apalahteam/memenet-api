const request = require('request');
module.exports.search = (giphy_api, q, rating, lang) => {
    console.log(rating)
    return new Promise((resolve, reject) => {
        let options = {
            'method': 'GET',
            'url': `${giphy_api.base_url}/search?api_key=${giphy_api.apikey}&q=${q}&lang=${lang}` + ((rating != null) ? `&rating=${rating}` : ''),
        };
        request(options, (error, response) => {
            if (error) reject(new Error(error));
            else {
                resolve(JSON.parse(response.body));
            }
        });
    });
};

module.exports.trending = (giphy_api, rating) => {
    return new Promise((resolve, reject) => {
        let options = {
            'method': 'GET',
            'url': `${giphy_api.base_url}/trending?api_key=${giphy_api.apikey}` + ((rating) ? `&rating=${rating}` : ''),
        };
        request(options, (error, response) => {
            if (error) reject(new Error(error));
            else {
                resolve(JSON.parse(response.body));
            }
        });
    });
};

module.exports.category = (giphy_api) => {
    return new Promise((resolve, reject) => {
        let options = {
            'method': 'GET',
            'url': `${giphy_api.base_url}/categories?api_key=${giphy_api.apikey}`,
        };
        request(options, (error, response) => {
            if (error) reject(new Error(error));
            else {
                resolve(JSON.parse(response.body));
            }
        });
    });
};

module.exports.autocomplete = (giphy_api, q) => {
    return new Promise((resolve, reject) => {
        let options = {
            'method': 'GET',
            'url': `${giphy_api.base_url}/search/tags?api_key=${giphy_api.apikey}&q=${q}`,
        };
        request(options, (error, response) => {
            if (error) reject(new Error(error));
            else {
                resolve(JSON.parse(response.body));
            }
        });
    });
};