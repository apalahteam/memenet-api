'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user_topup', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      id_user: {
        allowNull: false,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      id_user_transaction: {
        allowNull: false,
        defaultValue: 0,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      nominal: {
        allowNull: false,
        type: Sequelize.INTEGER(11)
      },
      message: {
        allowNull: false,
        type: Sequelize.STRING
      },

      // ALL FIELDS BELOW ARE REQUIRED
      created_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updated_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      deleted_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user_topup');
  }
};
