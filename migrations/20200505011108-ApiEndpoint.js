'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('api_endpoint', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      id_api_site: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        allowNull: false,
      },
      concat_url: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      req_query: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      req_body: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      req_header: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      // ALL FIELDS BELOW ARE REQUIRED
      created_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updated_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      deleted_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('api_endpoint');
  }
};