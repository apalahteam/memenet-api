'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('project_plan', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      id_project: {
        allowNull: false,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      id_plan: {
        allowNull: false,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      expires_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      is_active: {
        allowNull: false,
        type: Sequelize.INTEGER(1).UNSIGNED
      },
      remaining_hit: {
        allowNull: false,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      // ALL FIELDS BELOW ARE REQUIRED
      created_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updated_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      deleted_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('project_plan');
  }
};