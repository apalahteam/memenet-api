'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user_transaction', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      id_user: {
        allowNull: false,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      transaction_token: {
        allowNull: false,
        type: Sequelize.STRING
      },
      order_id: {
        allowNull: false,
        type: Sequelize.STRING
      },
      redirect_url: {
        allowNull: true,
        type: Sequelize.STRING
      },
      status: {
        allowNull: false,
        type: Sequelize.STRING
      },

      // ALL FIELDS BELOW ARE REQUIRED
      created_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updated_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      deleted_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user_transaction');
  }
};
