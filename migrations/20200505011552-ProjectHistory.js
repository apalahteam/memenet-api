'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('project_history', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      id_project: {
        allowNull: false,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      id_api_endpoint: {
        allowNull: false,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      status_code: {
        allowNull: false,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      message: {
        allowNull: false,
        type: Sequelize.STRING
      },
      response: {
        allowNull: false,
        type: Sequelize.TEXT('long')
      },
      // ALL FIELDS BELOW ARE REQUIRED
      created_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updated_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      deleted_by: {
        allowNull: true,
        type: Sequelize.INTEGER(11).UNSIGNED
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('project_history');
  }
};