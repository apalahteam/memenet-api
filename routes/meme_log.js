const Sequelize = require('sequelize');
const models = require('require-all')(__dirname + '/../models');

exports.insertLog = (req, res, data) => {
    Promise.all([createApiEndPoint(req, data), reduceProjectPlanHit(req)]).then(() => {
        res.status(200).send({
            data
        });
    }).catch((err) => res.status(500).send(err));
}

function createApiEndPoint(req, data) {
    return new Promise((resolve, reject) => {
        models.ApiEndpoint.create({
            id_api_site: req.meme.id,
            concat_url: req.path,
            req_query: JSON.stringify(req.query),
            req_body: JSON.stringify(req.body),
            req_header: JSON.stringify(req.headers)
        }).then(async (result) => {
            await createApiProjectHistory(req, result, data);
            resolve();
        }).catch((err) => {
            console.log(err)
            reject(err)
        })
    });
}

function createApiProjectHistory(req, result, data) {
    return new Promise((resolve, reject) => {
        models.ProjectHistory.create({
            id_project: req.project.id,
            id_api_endpoint: result.id,
            status_code: 200,
            message: '',
            response: JSON.stringify(data)
        }).then((result) => resolve()).catch((err) => reject(err))
    });
}

function reduceProjectPlanHit(req) {
    return new Promise((resolve, reject) => {
        models.ProjectPlan.update({
            remaining_hit: Sequelize.literal('remaining_hit - 1')
        }, {
            where: {
                id: req.project_plan.id
            }
        }).then((result) => resolve()).catch((err) => reject(err))
    });
}