var express = require('express');
var router = express.Router();
var models = require('require-all')(__dirname + '/../models');
const db = require('../config/database');
var mustBeAuthenticatedMiddleware = require("../middleware/mustBeAuthenticated");

/* GET home page. */
router.get('/', function (req, res, next) {
  models.User.findOne({
      include: ['UserTopUp']
    })
    .then(users => {
      console.log(users);
      res.status(200);
    })
    .catch(err => console.log(err));

  res.render('index', {
    title: 'Express'
  });
});
router.get('/login', function (req, res, next) {
  res.render('login');
});

// Route below requires authenticated user
// router.use(mustBeAuthenticatedMiddleware);
router.get('/dashboard', async function (req, res, next) {
  // const id_user = req.auth.id;
  const id_user = 1;
  // fetch project names and hits
  let data = await db.doQuery(`
    SELECT p.name, COUNT(1) AS hit_count
    FROM project p
      LEFT JOIN project_history ph ON p.id=ph.id_project
    WHERE p.id_user=?
    GROUP BY p.name;
  `, [id_user]);
  // change to array
  let labels = [];
  let hit_count = [];
  for (let i=0; i<data.length; i++) {
    labels.push(data[i].name);
    hit_count.push(data[i].hit_count);
  }
  res.render('dashboard', {labels: labels, hit_count: hit_count});
});
module.exports = router;