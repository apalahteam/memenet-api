var express = require('express');
var router = express.Router();
var models = require('require-all')(__dirname + '/../models');
const {
  check,
  validationResult,
  body
} = require('express-validator');

function getSHA256ofJSON(input) {
    return crypto.createHash('sha256').update(JSON.stringify(input)).digest('hex');
}

function randomString(length) {
  return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
}

router.post('/notify', [], async function (req, res, next) {
  
  let userTrans = await models.UserTransaction.findOne({
    where: {
      order_id: req.body.order_id
    }
  });

  if(userTrans == null){
    return res.status(404).json({
      "message": "Transaction not found :("
    });
  }
  
  models.UserTransaction.create({
    id_user: userTrans.id_user,
    transaction_token: req.body.transaction_id,
    order_id: req.body.order_id,
    redirect_url: "",
    status: req.body.transaction_status,
  }).then(async (trans) => {
    await userTrans.destroy();

    // Check if transaction complete
    if(req.body.transaction_status == "settlement"){
      models.UserTopUp.create({
        id_user: userTrans.id_user,
        id_user_transaction: trans.id,
        nominal: req.body.gross_amount,
        message: "Top up saldo subscription",
      }).then(async (topup) => {
        return res.status(200).json({
          "message": "OK"
        });
      }).catch(err => {
        return res.status(500).json({
          "message": "Error processing notification",
          "errors": err
        });
      });
    }

    return res.status(200).json({
      "message": "OK"
    });
  }).catch(err => {
    res.status(500).json({
      "message": "Error processing notification",
      "errors": err
    });
  });
});

router.post('/finish', [], function (req, res, next) {
  return res.status(200).json({
      "message": "Finish"
  });
});

router.post('/unfinish', [], function (req, res, next) {
  return res.status(200).json({
      "message": "Unfinish"
  });
});

router.post('/error', [], function (req, res, next) {
  return res.status(200).json({
      "message": "Error"
  });
});

module.exports = router;