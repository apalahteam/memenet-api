var express = require('express');
var router = express.Router();
var models = require('require-all')(__dirname + '/../models');
const {
  check,
  validationResult,
  body
} = require('express-validator');
const bcrypt = require('bcrypt');
const crypto = require("crypto");
var mustBeAuthenticatedMiddleware = require("../middleware/mustBeAuthenticated");

function getSHA256ofJSON(input) {
    return crypto.createHash('sha256').update(JSON.stringify(input)).digest('hex');
}

// Route below requires authenticated user
router.use(mustBeAuthenticatedMiddleware);

router.post('/', [
    // body('id_user')
    //  .isNumeric()
    //  .custom(async (value, {req} )=>{
    //     return await models.User.findAll({
    //       where: {
    //         id: value
    //       }
    //     })
    //     .then(users => {
    //       if (users.length <= 0) return Promise.reject("User not found");
    //     })
    //     .catch(err => {
    //       return Promise.reject(err);
    //     });
    // }),

    body('name')
     .isLength({ min: 1 }).withMessage('Name is required')
], (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
        "message": "Some data contain errors",
        "errors": errors.array()
        });
    }
    req.body.id_user = req.auth.id;
    req.body.appkey = getSHA256ofJSON(`${req.body.id_user}${Date.now()}`);
    req.body.created_by = req.auth.id;
    models.Project.create(req.body).then((project) => {
        res.status(201).json({
          "message": "Project created",
          "appkey": req.body.appkey
        });
        }).catch(err => {
            res.status(500).json({
            "message": "Error creating new project",
            "errors": err
        });
    });
});

router.put('/:id', [
    // check id_user
    // body('id_user')
    //  .isNumeric()
    //  .custom(async (value, {req} )=>{
    //     return await models.User.findAll({
    //       where: {
    //         id: value
    //       }
    //     })
    //     .then(users => {
    //       if (users.length <= 0) return Promise.reject("User not found");
    //     })
    //     .catch(err => {
    //       return Promise.reject(err);
    //     });
    // }),

    body('name')
     .isLength({ min: 1 }).withMessage('Name is required')
], async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
        "message": "Some data contain errors",
        "errors": errors.array()
        });
    }

    const id = req.params.id;

    // check project exist
    let project = await models.Project.findByPk(id);
    if (!project) return res.status(404).send({ message: 'Project not found' });
    
    // check only authorized owner can update
    if (req.auth.id != project.id_user) return res.status(403).send({ message: 'Action not allowed' });

    // update project
    // project.id_user = req.body.id_user;
    project.name = req.body.name;
    project.updated_by = req.auth.id;
    const result = await project.save();

    // error
    if (!result) {
        res.status(500).json({
            "message": "Error updating project",
            "errors": err
        });
    }

    // success
    return res.status(200).json({
        "message": "Project updated",
    });
});

router.delete('/:id', async (req, res, next) => {
    const id = req.params.id;

    // check project exist
    let project = await models.Project.findByPk(id);
    if (!project) return res.status(404).send({ message: 'Project not found' });

    // check only authorized owner can delete
    if (req.auth.id != project.id_user) return res.status(403).send({ message: 'Action not allowed' });

    // delete project
    project.deleted_by = req.auth.id;
    await project.save();
    const result = await project.destroy();

    // error
    if (!result) {
        res.status(500).json({
            "message": "Error deleting project",
            "errors": err
        });
    }

    // success
    return res.status(200).json({
        "message": "Project deleted",
    });
});

router.get('/', async (req, res, next) => {
    let projects = await models.Project.findAll({
      attributes: ['id','id_user', 'name', 'appkey', 'created_at', 'updated_at'],
    });
    const id_user = req.query.user;
    const q = req.query.q;

    // filtering
    if (id_user) projects = projects.filter(project => { return project.id_user == id_user });
    if (q) projects = projects.filter(project => { return project.name.includes(q) });
    return res.status(200).send(projects);
});

router.get('/:id', async (req, res, next) => {
    let project = await models.Project.findOne({
      attributes: ['id','id_user', 'name', 'appkey', 'created_at', 'updated_at'],
      where: {
          id: req.params.id
      }
    });
    return res.status(200).send(project);
});

module.exports = router;