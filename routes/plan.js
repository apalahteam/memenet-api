var express = require('express');
var router = express.Router();
var models = require('require-all')(__dirname + '/../models');
const {
  check,
  validationResult,
  body
} = require('express-validator');
var mustBeAuthenticatedMiddleware = require("../middleware/mustBeAuthenticated");

router.get('/', async (req, res, next) => {
    let plans = await models.Plan.findAll({
        attributes: ['id', 'name', 'price', 'duration', 'max_hit_month']
    });
    const q = req.query.q;

    // filtering
    if (q) plans = plans.filter(plan => { return plan.name.includes(q) });
    return res.status(200).send(plans);
});

router.get('/:id', async (req, res, next) => {
    let plans = await models.Plan.findOne({
        attributes: ['id', 'name', 'price', 'duration', 'max_hit_month'],
        where: {
            id: req.params.id
        }
    });
    return res.status(200).send(plans);
});

// Route below requires authenticated user
router.use(mustBeAuthenticatedMiddleware);

router.post('/', [
    body('name')
     .isLength({ min: 1 }).withMessage('Name is required'),
    
    body('price')
     .isLength({ min: 1 }).withMessage('Price is required')
     .isNumeric().withMessage('Price has to be numeric'),

    body('duration')
     .isLength({ min: 1 }).withMessage('Duration is required')
     .isNumeric().withMessage('Duration has to be numeric'),

    // body('max_hit_hour')
    //  .isLength({ min: 1 }).withMessage('Max hit hour is required')
    //  .isNumeric().withMessage('Max hit hour has to be numeric'),

    body('max_hit_month')
     .isLength({ min: 1 }).withMessage('Max hit month is required')
     .isNumeric().withMessage('Max hit month has to be numeric'),
], (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
        "message": "Some data contain errors",
        "errors": errors.array()
        });
    }
    
    // check only admin can create
    if (req.auth.id_user_level != 1) return res.status(403).send({ message: 'Action not allowed' });

    models.Plan.create(req.body).then((project) => {
        res.status(201).json({
          "message": "Plan created"
        });
        }).catch(err => {
            res.status(500).json({
            "message": "Error creating new plan",
            "errors": err
        });
    });
});

router.put('/:id', [
  body('name')
   .isLength({ min: 1 }).withMessage('Name is required'),
  
  body('price')
   .isLength({ min: 1 }).withMessage('Price is required')
   .isNumeric().withMessage('Price has to be numeric'),

  body('duration')
   .isLength({ min: 1 }).withMessage('Duration is required')
   .isNumeric().withMessage('Duration has to be numeric'),

//   body('max_hit_hour')
//    .isLength({ min: 1 }).withMessage('Max hit hour is required')
//    .isNumeric().withMessage('Max hit hour has to be numeric'),

  body('max_hit_month')
   .isLength({ min: 1 }).withMessage('Max hit month is required')
   .isNumeric().withMessage('Max hit month has to be numeric'),
], async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
        "message": "Some data contain errors",
        "errors": errors.array()
        });
    }
    
    // check only admin can update
    if (req.auth.id_user_level != 1) return res.status(403).send({ message: 'Action not allowed' });

    const id = req.params.id;

    // check project exist
    let plan = await models.Plan.findByPk(id);
    if (!plan) return res.status(404).send({ message: 'Plan not found' });

    // update project
    plan.name = req.body.name;
    plan.price = req.body.price;
    plan.duration = req.body.duration;
    plan.max_hit_hour = req.body.max_hit_hour;
    plan.max_hit_month = req.body.max_hit_month;
    const result = await plan.save();

    // error
    if (!result) {
        res.status(500).json({
            "message": "Error updating plan",
            "errors": err
        });
    }

    // success
    return res.status(200).json({
        "message": "Plan updated",
    });
});

router.delete('/:id', async (req, res, next) => {
    // check only admin can delete
    if (req.auth.id_user_level != 1) return res.status(403).send({ message: 'Action not allowed' });

    const id = req.params.id;

    // check project exist
    let plan = await models.Plan.findByPk(id);
    if (!plan) return res.status(404).send({ message: 'Plan not found' });

    // delete project
    const result = await plan.destroy();

    // error
    if (!result) {
        res.status(500).json({
            "message": "Error deleting plan",
            "errors": err
        });
    }

    // success
    return res.status(200).json({
        "message": "Plan deleted",
    });
});

module.exports = router;