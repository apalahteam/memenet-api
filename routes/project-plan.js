var express = require('express');
var router = express.Router();
var models = require('require-all')(__dirname + '/../models');
const {
  check,
  validationResult,
  body
} = require('express-validator');
const db = require('../config/database');
var mustBeAuthenticatedMiddleware = require("../middleware/mustBeAuthenticated");

const ACTIVE_PERIOD = 30;
const SUBSCRIPTION_DUE_WARNING_DAYS = 5;

// Route below requires authenticated user
router.use(mustBeAuthenticatedMiddleware);

router.post('/', [
    body('id_project')
     .isLength({ min: 1 }).withMessage('Project ID is required')
     .custom(async (value, {req}) => {
        return await models.Project.findByPk(value).then(project => {
          if (!project) return Promise.reject("Project not found");
        }).catch(err => {
          return Promise.reject(err);
        });
     }),

     body('id_plan')
     .isLength({ min: 1 }).withMessage('Plan ID is required')
     .custom(async (value, {req}) => {
        return await models.Plan.findByPk(value).then(plan => {
          if (!plan) return Promise.reject("Plan not found");
        }).catch(err => {
          return Promise.reject(err);
        });
     }),
], async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
        "message": "Some data contain errors",
        "errors": errors.array()
        });
    }

    const project = await models.Project.findByPk(req.body.id_project);
    const user = await models.User.findByPk(project.id_user);
    const plan = await models.Plan.findByPk(req.body.id_plan);

    // count user's cash
    let saldo = 0;
    saldo = await db.doQuery(`SELECT IFNULL(SUM(nominal),0) AS saldo FROM user_topup WHERE id_user=${user.id}`);
    saldo = saldo[0].saldo;

    // check enough cash
    if (plan.price > saldo) return res.status(400).send('Not enough cash, please top up first');

    // minus user cash
    await models.UserTopUp.create({
      id_user: project.id_user,
      nominal: parseInt(plan.price) * -1,
      message: 'Subscribe plan'
    }).then((topUp) => {}).catch(err => {
      res.status(500).json({
        "message": "Internal server error",
        "errors": err
      });
    });

    // set expires_at
    expireDate = new Date();
    expireDate.setDate(expireDate.getDate() + ACTIVE_PERIOD);
    req.body.expires_at = expireDate;

    // set is_active
    let activePlan = await models.ProjectPlan.findAll({
      where: {
        id_project: project.id,
        is_active: 1
      }
    });
    req.body.is_active = (activePlan.length <= 0) ? 1 : 0;

    // set remaining hit
    req.body.remaining_hit = plan.max_hit_month;

    // insert project plan
    models.ProjectPlan.create(req.body).then((project) => {
        res.status(201).json({
          "message": "Project plan subscribed"
        });
      }).catch(err => {
        res.status(500).json({
          "message": "Error inserting project plan",
          "errors": err
        });
      });
});

router.put('/:id', [
  body('id_project')
   .isLength({ min: 1 }).withMessage('Project ID is required')
   .custom(async (value, {req}) => {
      return await models.Project.findByPk(value).then(project => {
        if (!project) return Promise.reject("Project not found");
      }).catch(err => {
        return Promise.reject(err);
      });
   }),

   body('id_plan')
   .isLength({ min: 1 }).withMessage('Plan ID is required')
   .custom(async (value, {req}) => {
      return await models.Plan.findByPk(value).then(plan => {
        if (!plan) return Promise.reject("Plan not found");
      }).catch(err => {
        return Promise.reject(err);
      });
   }),
], async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
        "message": "Some data contain errors",
        "errors": errors.array()
        });
    }

    const id = req.params.id;

    // check project plan exist
    let projectPlan = await models.ProjectPlan.findByPk(id);
    if (!projectPlan) return res.status(404).send({ message: 'Project plan not found' });

    // update project plan
    projectPlan.id_project = req.body.id_project;
    projectPlan.id_plan = req.body.id_plan;
    const result = await projectPlan.save();

    // error
    if (!result) {
        res.status(500).json({
            "message": "Error updating project plan",
            "errors": err
        });
    }

    // success
    return res.status(200).json({
        "message": "Project plan updated",
    });
});

router.delete('/:id', async (req, res, next) => {
    const id = req.params.id;

    // check project plan exist
    let projectPlan = await models.ProjectPlan.findByPk(id);
    if (!projectPlan) return res.status(404).send({ message: 'Project plan not found' });

    // delete project plan
    const result = await project.destroy();

    // error
    if (!result) {
        res.status(500).json({
            "message": "Error deleting project plan",
            "errors": err
        });
    }

    // success
    return res.status(200).json({
        "message": "Project plan deleted",
    });
});

router.get('/history', async (req, res, next) => {
  const id_user = (req.auth.id_user_level == 1) ? req.query.user : req.auth.id;
  const id_project = req.query.project;

  // create filter
  let filterProject = {};
  if (id_project) filterProject.id_project = id_project;
  let filterUser = {};
  if (id_user) filterUser.id_user = id_user;

  let projectPlans = await models.ProjectPlan.findAll({
    attributes: ['id', 'id_project', 'id_plan', 'expires_at', 'is_active', 'remaining_hit'],
    where: filterProject,
    order: [['created_at', 'DESC']],
    include: [
      {
        attributes: ['id','id_user', 'name', 'appkey', 'created_at', 'updated_at'],
        where: filterUser,
        model: models.Project,
        as: 'Project',
        required: false,
      },
      {
        attributes: ['id', 'name', 'price', 'duration', 'max_hit_month'],
        model: models.Plan,
        as: 'Plan',
        required: false,
      }
    ]
  });

  let data = {};
  data.data_count = projectPlans.length;
  data.project_plans = projectPlans;

  // H-5 subscription ends warning
  if (id_user) {
    for (let i=0; i<projectPlans.length; i++) {
      if (projectPlans[i].is_active == 1) {
        let currentDay = new Date().getTime();
        let dueDay = new Date(projectPlans[i].expires_at).getTime();
        let dueMinusWarningDay = new Date(projectPlans[i].expires_at);
        dueMinusWarningDay.setDate(dueMinusWarningDay.getDate() - SUBSCRIPTION_DUE_WARNING_DAYS);
        dueMinusWarningDay = dueMinusWarningDay.getTime();
        if (currentDay >= dueMinusWarningDay && currentDay <= dueDay) {
          daysToDue = parseInt((dueDay - currentDay) / (1000 * 3600 * 24));
          data.warning = `Your latest subscription will end in ${daysToDue} day(s)`;
        }
        break;
      }
    }
  }
  return res.status(200).send(data);
});

router.get('/history/:id', async (req, res, next) => {

  let projectPlans = await models.ProjectPlan.findOne({
    attributes: ['id', 'id_project', 'id_plan', 'expires_at', 'is_active', 'remaining_hit'],
    where: { id: req.params.id },
    include: [
      {
        attributes: ['id','id_user', 'name', 'appkey', 'created_at', 'updated_at'],
        model: models.Project,
        as: 'Project'
      },
      {
        attributes: ['id', 'name', 'price', 'duration', 'max_hit_month'],
        model: models.Plan,
        as: 'Plan'
      }
    ]
  });
  if (projectPlans.Project.id_user != req.auth.id) return res.status(403).send({ message: 'Forbidden' });
  return res.status(200).send(projectPlans);
});

module.exports = router;