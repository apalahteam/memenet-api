const express = require("express");
const router = express.Router();

const apiMeme = require("../api/giphy");

// api.giphy.com/v1/gifs/search?<parameters>
// api.giphy.com/v1/gifs/trending?<parameters>
// api.giphy.com/v1/gifs/categories?<parameters>
// api.giphy.com/v1/gifs/search/tags?q=<term>&key=<API KEY
const mustBeHaveAppKey = require('../middleware/mustBeHaveAppKey');
const MemeLog = require('./meme_log');

router.use(mustBeHaveAppKey);

router.get("/", (req, res, next) => {
    res.send("Once again, the world saved by meme");
});
router
    .get("/search", async (req, res, next) => {
        const {
            q,
            rating,
            lang
        } = req.query;
        let data = await apiMeme.search(
            req.meme,
            q ? q : '',
            rating ? rating : null,
            lang ? lang : "en"
        );
        MemeLog.insertLog(req, res, data);
    })
    .get("/trending", async (req, res, next) => {
        const {
            rating
        } = req.query;
        let data = await apiMeme.trending(
            req.meme,
            rating ? rating : null,
        );
        MemeLog.insertLog(req, res, data);
    })
    .get("/categories", async (req, res, next) => {
        let data = await apiMeme.category(req.meme);
        MemeLog.insertLog(req, res, data);
    })
    .get("/autocomplete", async (req, res, next) => {
        const {
            q
        } = req.query;
        let data = await apiMeme.autocomplete(
            req.meme,
            q ? q : ''
        );
        MemeLog.insertLog(req, res, data);
    });
module.exports = router;