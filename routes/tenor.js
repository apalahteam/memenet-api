const express = require("express");
const router = express.Router();

const apiMeme = require("../api/tenor");

// https://api.tenor.com/v1/search?<parameters>
// https://api.tenor.com/v1/trending?<parameters>
// https://api.tenor.com/v1/categories?<parameters>
// https://api.tenor.com/v1/autocomplete?q=<term>&key=<API KEY
const mustBeHaveAppKey = require('../middleware/mustBeHaveAppKey');
const MemeLog = require('./meme_log');

router.use(mustBeHaveAppKey);
router.get("/", (req, res, next) => {
    res.status(200).send("Once again, the world saved by meme");
});
router
    .get("/search", async (req, res, next) => {
        const {
            q,
            filter,
            locale
        } = req.query;
        let data = await apiMeme.search(
            req.meme,
            q ? q : '',
            filter ? filter : "off",
            locale ? locale : "en_US"
        );
        MemeLog.insertLog(req, res, data);
        // res.status(200).send({
        //     results: data.results
        // });
    })
    .get("/trending", async (req, res, next) => {
        const {
            filter,
            locale
        } = req.query;
        let data = await apiMeme.trending(
            req.meme,
            filter ? filter : "off",
            locale ? locale : "en_US"
        );
        MemeLog.insertLog(req, res, data);
    })
    .get("/categories", async (req, res, next) => {
        const {
            filter,
            locale
        } = req.query;
        let data = await apiMeme.category(
            req.meme,
            filter ? filter : "off",
            locale ? locale : "en_US"
        );
        MemeLog.insertLog(req, res, data);
    })
    .get("/autocomplete", async (req, res, next) => {
        const {
            q,
            filter,
            locale
        } = req.query;
        let data = await apiMeme.autocomplete(
            req.meme,
            q ? q : '',
            filter ? filter : "off",
            locale ? locale : "en_US"
        );
        MemeLog.insertLog(req, res, data);
    });

module.exports = router;