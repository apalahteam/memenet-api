var express = require('express');
var router = express.Router();
var models = require('require-all')(__dirname + '/../models');
const {
  check,
  validationResult,
  body
} = require('express-validator');
const bcrypt = require('bcrypt');
const crypto = require("crypto");
const jwt = require('jsonwebtoken');
const midtrans = require('../config/midtrans');
const {
  Op
} = require("sequelize");

var mustBeAuthenticatedMiddleware = require("../middleware/mustBeAuthenticated");

function getSHA256ofJSON(input) {
  return crypto.createHash('sha256').update(JSON.stringify(input)).digest('hex');
}

function randomString(length) {
  return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
}

router.post('/register', [

  // validate email
  body('email')
  .isEmail()
  .normalizeEmail()
  .custom(async (value, {
    req
  }) => {
    return await models.User.findAll({
        where: {
          email: value
        }
      })
      .then(users => {
        if (users.length != 0) return Promise.reject("Email already used");
      })
      .catch(err => {
        return Promise.reject(err);
      });
  }),

  // validate password
  body('password').custom((value, {
    req
  }) => {
    if (value.length < 6) {
      throw new Error('Password should be minimum 6 characters');
    }
    return true;
  }),

  // validate password_confirmation
  body('password_confirmation').custom((value, {
    req
  }) => {
    if (value !== req.body.password) {
      throw new Error('Password confirmation does not match password');
    }
    return true;
  }),

  // validate name
  // body('name')
  // .isAlpha().withMessage('Must be filled and only with alphabetical chars')

], function (req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      "message": "Some data contain errors",
      "errors": errors.array()
    });
  }
  req.body.id_user_level = 2;
  req.body.password = bcrypt.hashSync(req.body.password, 10);
  models.User.create(req.body).then((user) => {
    res.status(201).json({
      "message": "User created"
    });
  }).catch(err => {
    res.status(500).json({
      "message": "Error creating new user",
      "errors": err
    });
  });
});

router.post('/login', [

  // validate email
  body('email')
  .isEmail()
  .normalizeEmail(),

  // validate password
  body('password').custom((value, {
    req
  }) => {
    if (value.length < 6) {
      throw new Error('Password should be minimum 6 characters');
    }
    return true;
  })

], async function (req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      "message": "Some data contain errors",
      "errors": errors.array()
    });
  }

  let user = await models.User.findOne({
    where: {
      email: req.body.email
    }
  });

  if (user == null) {
    return res.status(422).json({
      "message": "Credentials doesn't match in our record"
    });
  }

  // Start the authentication process
  if (!bcrypt.compareSync(req.body.password, user.password)) {
    return res.status(422).json({
      "message": "Credentials doesn't match in our record"
    });
  }

  models.UserLogin.create({
    'id_user': user.id,
    'secret': getSHA256ofJSON({
      'user_id': user.id,
      'time': new Date(),
      'salt': randomString(5)
    }),
    'created_by': user.id
  }).then((ulogin) => {
    var token = jwt.sign({
      "user_id": user.id,
      "trace_id": ulogin.id,
    }, ulogin.secret);
    res.status(201).json({
      "message": "Login Successful",
      "token": token
    });
  }).catch(err => {
    res.status(500).json({
      "message": "Login Error",
      "errors": err
    });
  });
});

// Route below requires authenticated user
router.use(mustBeAuthenticatedMiddleware);

router.get('/', [], function (req, res, next) {
  res.status(200).json({
    "message": "OK",
    "data": {
      "id": req.auth.id,
      "email": req.auth.email,
      "name": req.auth.name
    }
  });
});

router.put('/', [

  // validate email
  body('email')
  .isEmail()
  .normalizeEmail()
  .custom(async (value, {
    req
  }) => {
    return await models.User.findAll({
        where: {
          email: value,
          id: {
            [Op.not]: req.auth.id
          }
        }
      })
      .then(users => {
        if (users.length != 0) return Promise.reject("Email already used");
      })
      .catch(err => {
        return Promise.reject(err);
      });
  }),

  // validate name
  body('name')
  .isAlpha().withMessage('Must be filled and only with alphabetical chars')

], function (req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      "message": "Some data contain errors",
      "errors": errors.array()
    });
  }
  req.auth.email = req.body.email;
  req.auth.name = req.body.name;

  req.auth.save().then((user) => {
    res.status(200).json({
      "message": "User data updated"
    });
  }).catch(err => {
    res.status(500).json({
      "message": "Error updating user data",
      "errors": err
    });
  });
});


router.put('/changePassword', [

  // validate old password
  body('old_password')
  .custom(async (value, {
    req
  }) => {
    if (!bcrypt.compareSync(req.body.old_password, req.auth.password)) {
      return Promise.reject("Old password doesn't match");
    }
  }),

  // validate password
  body('new_password').custom((value, {
    req
  }) => {
    if (value.length < 6) {
      throw new Error('Password should be minimum 6 characters');
    }
    return true;
  }),

  // validate password_confirmation
  body('new_password_confirmation').custom((value, {
    req
  }) => {
    if (value !== req.body.new_password) {
      throw new Error('Password confirmation does not match new password');
    }
    return true;
  }),

], function (req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      "message": "Some data contain errors",
      "errors": errors.array()
    });
  }
  req.auth.password = bcrypt.hashSync(req.body.new_password, 10);

  req.auth.save().then((user) => {
    res.status(200).json({
      "message": "User password updated"
    });
  }).catch(err => {
    res.status(500).json({
      "message": "Error updating user password",
      "errors": err
    });
  });
});

router.delete('/', [], function (req, res, next) {
  req.auth.destroy().then((user) => {
    res.status(200).json({
      "message": "User account deactivated"
    });
  }).catch(err => {
    res.status(500).json({
      "message": "Error deactivating user",
      "errors": err
    });
  });
});

router.get('/transaction', [], async function (req, res, next) {
  let topUp = await models.UserTopUp.findAll({
    where: {
      id_user: req.auth.id
    },
    // attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt'], include: ['id', 'idtrans']}
    attributes: {
      include: [
        ['id', 'transaction_id'],
        ['created_at', 'time']
      ],
      exclude: ['id', 'id_user', 'id_user_transaction', 'createdAt', 'updatedAt', 'deletedAt']
    }
  });

  res.status(200).json({
    "message": "OK",
    "data": topUp
  });

});

router.post('/topup', [

  // validate email
  body('amount')
  .isNumeric()
  .isDecimal(),

], async (req, res, next) => {
  let dt = new Date();
  let parameter = {
    "transaction_details": {
      "order_id": `${process.env.APP_NAME}_TOPUP_${dt.getFullYear()}${(dt.getMonth()+1)}${dt.getDate()}_${dt.getHours()}${dt.getMinutes()}${dt.getSeconds()}`,
      "gross_amount": req.body.amount
    },
    "item_details": [{
      "id": "TP-001",
      "price": req.body.amount,
      "quantity": 1,
      "name": "Top Up saldo MemeNet",
      "brand": "MemeNet",
      "category": "Digital Payment",
      "merchant_name": "MemeNet"
    }],
    "customer_details": {
      "first_name": req.auth.name,
      "last_name": "",
      "email": req.auth.email,
    },
    "enabled_payments": ["credit_card", "mandiri_clickpay", "cimb_clicks", "bca_klikbca", "bca_klikpay", "bri_epay", "echannel", "indosat_dompetku", "mandiri_ecash", "permata_va", "bca_va", "bni_va", "other_va", "gopay", "kioson", "indomaret", "gci", "danamon_online"],
    "callbacks": {
      "finish": "https://demo.midtrans.com"
    },
    // "custom_field1": "custom field 1 content",
    // "custom_field2": "custom field 2 content",
    // "custom_field3": "custom field 3 content"
  };

  midtrans.snap.createTransaction(parameter)
    .then((transaction) => {
      models.UserTransaction.create({
        id_user: req.auth.id,
        transaction_token: transaction.token,
        order_id: parameter.transaction_details.order_id,
        redirect_url: transaction.redirect_url,
        status: "create",
      }).then((trans) => {
        return res.status(200).json({
          "message": "Transaction created!",
          "data": {
            'redirect_url': transaction.redirect_url,
          }
        });
      }).catch(err => {
        res.status(500).json({
          "message": "Error creating transaction",
          "errors": err
        });
      });
    })
    .catch((e) => {
      return res.status(500).json({
        "message": "Oops... something went wrong",
        "error": e.message
      });
    });

});

module.exports = router;