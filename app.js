var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const cors = require("cors");

var requestInterceptorMiddleware = require("./middleware/requestInterceptor");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var midtransRouter = require("./routes/midtrans");
var tenorRouter = require("./routes/tenor");
var giphyRouter = require("./routes/giphy");
var projectRouter = require("./routes/project");
var planRouter = require("./routes/plan");
var projectPlanRouter = require("./routes/project-plan");

var app = express();
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.set('trust proxy', true)

app.use(logger("dev"));
app.use(cors());
app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(requestInterceptorMiddleware);

// [BEGIN] ROUTE HANDLING
app.use("/", indexRouter);
app.use("/api/user", usersRouter);

app.use("/api/midtrans", midtransRouter);

app.use("/api/meme/tenor", tenorRouter);
app.use("/api/meme/giphy", giphyRouter);

app.use("/api/project", projectRouter);
app.use("/api/plan", planRouter);
app.use("/api/project/plan", projectPlanRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;