function limitStr(input, max, end = '...') {
    if (input.length > max) input = input.substring(0, max) + end;
    return input;
}

function toMonthName(input, type) {
    var lsMonthLong = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var lsMonthShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    if (type == 'long') return lsMonthLong[parseInt(input) - 1];
    else if (type == 'short') return lsMonthShort[parseInt(input) - 1];
}

function parseGetQuery(link, params) {
    var parse = link + "?";
    for (var index in params) {
        parse += index + "=" + params[index] + "&";
    }
    return parse;
}

function eventFire(el, etype) {
    if (el.fireEvent) {
        el.fireEvent('on' + etype);
    } else {
        var evObj = document.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
    }
}

function copyToClipboard(strVal) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(strVal).select();
    document.execCommand("copy");
    $temp.remove();
}

function updateQueryStringParam(key, value) {

    var baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
        urlQueryString = document.location.search,
        newParam = key + '=' + value,
        params = '?' + newParam;

    // If the "search" string exists, then build params from it
    if (urlQueryString) {
        var updateRegex = new RegExp('([\?&])' + key + '[^&]*');
        var removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

        if (typeof value == 'undefined' || value == null || value == '') { // Remove param if value is empty
            params = urlQueryString.replace(removeRegex, "$1");
            params = params.replace(/[&;]$/, "");

        } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it
            params = urlQueryString.replace(updateRegex, "$1" + newParam);

        } else { // Otherwise, add it to end of query string
            params = urlQueryString + '&' + newParam;
        }
    }

    // no parameter was set so we don't need the question mark
    params = params == '?' ? '' : params;

    window.history.replaceState({}, "", baseUrl + params);
};
