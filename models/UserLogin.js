'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const UserLogin = db.sequelize.define('UserLogin', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_user: Sequelize.INTEGER(11).UNSIGNED,
  secret: Sequelize.STRING,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'user_login',
});

module.exports = UserLogin;