'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const User = db.sequelize.define('User', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_user_level: Sequelize.INTEGER,
  email: Sequelize.STRING,
  password: Sequelize.STRING,
  name: Sequelize.STRING
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'user',
});

// associations can be defined here
User.belongsTo(models.UserLevel, {targetKey: 'id', foreignKey: 'id_user_level', as: 'UserLevel'});
models.UserLevel.hasMany(User, {foreignKey: 'id_user_level', as: 'User'});

models.UserTopUp.belongsTo(User, {targetKey: 'id', foreignKey: 'id_user', as: 'User'});
User.hasMany(models.UserTopUp, {foreignKey: 'id_user', as: 'UserTopUp'});

models.UserTransaction.belongsTo(User, {targetKey: 'id', foreignKey: 'id_user', as: 'User'});
User.hasMany(models.UserTransaction, {foreignKey: 'id_user', as: 'UserTransaction'});

models.UserLogin.belongsTo(User, {targetKey: 'id', foreignKey: 'id_user', as: 'User'});
User.hasMany(models.UserLogin, {foreignKey: 'id_user', as: 'UserLogin'});

module.exports = User;