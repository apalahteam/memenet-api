'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const Project = db.sequelize.define('Project', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_user: Sequelize.INTEGER(11).UNSIGNED,
  name: Sequelize.STRING,
  appkey: Sequelize.STRING
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'project',
});

Project.belongsTo(models.User, {targetKey: 'id', foreignKey: 'id_user', as: 'User'});
models.User.hasMany(Project, {foreignKey: 'id_user', as: 'Project'});

models.ProjectPlan.belongsTo(Project, {targetKey: 'id', foreignKey: 'id_project', as: 'Project'});
Project.hasMany(models.ProjectPlan, {foreignKey: 'id_project', as: 'ProjectPlan'});

models.ProjectHistory.belongsTo(Project, {targetKey: 'id', foreignKey: 'id_project', as: 'Project'});
Project.hasMany(models.ProjectHistory, {foreignKey: 'id_project', as: 'ProjectHistory'});

module.exports = Project;