'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const ProjectHistory = db.sequelize.define('ProjectHistory', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_project: Sequelize.INTEGER(11).UNSIGNED,
  id_api_endpoint: Sequelize.INTEGER(11).UNSIGNED,
  status_code: Sequelize.INTEGER(11).UNSIGNED,
  message: Sequelize.STRING,
  response: Sequelize.TEXT('long')
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'project_history',
});
ProjectHistory.associate = function (models) {
  // associations can be defined here
};

module.exports = ProjectHistory;