'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const ProjectPlan = db.sequelize.define('ProjectPlan', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_project: Sequelize.INTEGER(11).UNSIGNED,
  id_plan: Sequelize.INTEGER(11).UNSIGNED,
  expires_at: Sequelize.DATE,
  is_active: Sequelize.INTEGER(1).UNSIGNED,
  remaining_hit: Sequelize.INTEGER(11).UNSIGNED,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'project_plan',
});

ProjectPlan.belongsTo(models.Plan, {targetKey: 'id', foreignKey: 'id_plan', as: 'Plan'});
models.Plan.hasMany(ProjectPlan, {foreignKey: 'id_plan', as: 'ProjectPlan'});

module.exports = ProjectPlan;