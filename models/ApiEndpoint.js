'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const ApiEndPoint = db.sequelize.define('ApiEndPoint', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_api_site: Sequelize.INTEGER(11).UNSIGNED,
  concat_url: Sequelize.STRING,
  req_query: Sequelize.STRING,
  req_body: Sequelize.STRING,
  req_header: Sequelize.STRING,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'api_endpoint',
});

ApiEndPoint.belongsTo(models.ApiSite, {targetKey: 'id', foreignKey: 'id_api_site', as: 'ApiSite'});
models.ApiSite.hasMany(ApiEndPoint, {foreignKey: 'id_api_site', as: 'ApiEndPoint'});

models.ProjectHistory.belongsTo(ApiEndPoint, {targetKey: 'id', foreignKey: 'id_api_endpoint', as: 'ApiEndPoint'});
ApiEndPoint.hasMany(models.ProjectHistory, {foreignKey: 'id_api_endpoint', as: 'ProjectHistory'});

module.exports = ApiEndPoint;