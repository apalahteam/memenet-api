'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const UserTopUp = db.sequelize.define('UserTopUp', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_user: Sequelize.INTEGER(11).UNSIGNED,
  nominal: Sequelize.INTEGER(11),
  message: Sequelize.STRING,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'user_topup',
});

UserTopUp.belongsTo(models.UserTransaction, {targetKey: 'id', foreignKey: 'id_user_transaction', as: 'UserTransaction'});
models.UserTransaction.hasMany(UserTopUp, {foreignKey: 'id_user_transaction', as: 'UserTopUp'});

module.exports = UserTopUp;