'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const UserLevel = db.sequelize.define('UserLevel', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  name: Sequelize.STRING,
  level: Sequelize.INTEGER(11).UNSIGNED,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'user_level',
});

// associations can be defined here

module.exports = UserLevel;