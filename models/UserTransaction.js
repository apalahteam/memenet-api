'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const UserTransaction = db.sequelize.define('UserTransaction', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_user: Sequelize.INTEGER(11).UNSIGNED,
  transaction_token: Sequelize.STRING,
  order_id: Sequelize.STRING,
  redirect_url: Sequelize.STRING,
  status: Sequelize.STRING,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'user_transaction',
});

module.exports = UserTransaction;