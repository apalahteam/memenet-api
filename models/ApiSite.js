'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');

const ApiSite = db.sequelize.define('ApiSite', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  name: Sequelize.STRING,
  base_url: Sequelize.STRING,
  apikey: Sequelize.STRING,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'api_site',
});
ApiSite.associate = function (models) {
  // associations can be defined here
};

module.exports = ApiSite;