'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const InboundDump = db.sequelize.define('InboundDump', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  ip_address: Sequelize.STRING,
  request_header: Sequelize.TEXT,
  request_body: Sequelize.TEXT,
  request_param: Sequelize.TEXT
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'inbound_dump',
});

module.exports = InboundDump;