'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');

const Plan = db.sequelize.define('Plan', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  name: Sequelize.STRING,
  price: Sequelize.INTEGER(11).UNSIGNED,
  duration: Sequelize.INTEGER(11).UNSIGNED,
  max_hit_month: Sequelize.INTEGER(11).UNSIGNED,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'plan',
});
Plan.associate = function (models) {
  // associations can be defined here
};

module.exports = Plan;