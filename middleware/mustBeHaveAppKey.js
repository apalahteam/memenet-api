var models = require("require-all")(__dirname + "/../models");

module.exports = async (req, res, next) => {
  console.log(req.originalUrl);
  const { appkey } = req.query;
  if (!appkey) {
    return res.status(401).json({
      message: "Unauthorized",
    });
  }

  const project = await models.Project.findOne({
    where: {
      appkey: appkey,
    },
  });

  if (!project) {
    return res.status(404).json({
      message: "Project Not Found",
    });
  }
  const project_plan = await models.ProjectPlan.findOne({
    where: {
      id_project: project.id,
      is_active: 1,
    },
  });

  if (!project_plan) {
    return res.status(404).json({
      message: "Your Project don't have any active plan",
    });
  }

  if (project_plan.remaining_hit === 0) {
    return res.status(400).json({
      message: "Your Active Plan have 0 remaining hit",
    });
  }
  let meme_name = "asdasd";
  if (req.originalUrl.indexOf("tenor") > -1) {
    meme_name = "tenor";
  }
  if (req.originalUrl.indexOf("giphy") > -1) {
    meme_name = "giphy";
  }

  const meme = await models.ApiSite.findOne({
    where: {
      name: meme_name,
    },
  });
  if (!meme) {
    return res.status(400).json({
      message: "We don't have that kind meme API",
    });
  }

  req.meme = meme;
  req.project = project;
  req.project_plan = project_plan;
  next();
};
