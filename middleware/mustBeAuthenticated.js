var models = require('require-all')(__dirname + '/../models');
const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
    if(typeof req.headers.authorization === 'undefined' || req.headers.authorization == null || req.headers.authorization == ''){
        res.status(401).json({
            "message": "Unauthorized"
        });
    }

    split = req.headers.authorization.split('.');

    obj = JSON.parse(Buffer.from(split[1], 'base64').toString());
    
    let log = await models.UserLogin.findOne({
        where: {
          id: obj.trace_id
        }
    });

    if(log == null){
        res.status(401).json({
            "message": "Unauthorized"
        });
    }

    jwt.verify(req.headers.authorization, log.secret, async function(err, decoded) {
        if(err){
            res.status(401).json({
                "message": "Unauthorized",
                "err": err
            });
            return;
        }

        req.auth = await models.User.findByPk(decoded.user_id);
        next();
    });
}